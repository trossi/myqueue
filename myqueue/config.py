"""User configuration (handling of .myqueue/config.py files)."""
import subprocess
import warnings
from math import inf
from pathlib import Path
from typing import Any, Dict, List, Set, Tuple


class Configuration:
    def __init__(self,
                 scheduler: str,
                 nodes: List[Tuple[str, Dict[str, Any]]] = None,
                 parallel_python: str = 'python3',
                 mpiexec: str = 'mpiexec',
                 extra_args: List[str] = None,
                 maximum_diskspace: float = inf,
                 notifications: Dict[str, str] = None,
                 mpi_implementation: str = None,
                 home: Path = None):
        """Configuration object.

        """
        self.scheduler = scheduler
        self.nodes = nodes or []
        self.parallel_python = parallel_python
        self.mpiexec = mpiexec
        self.extra_args = extra_args or []
        self.maximum_diskspace = maximum_diskspace
        self.notifications = notifications or {}
        self._mpi_implementation = mpi_implementation
        self.home = home or Path.cwd()

    def __repr__(self) -> str:
        args = ', '.join(f'{name.lstrip("_")}={getattr(self, name)!r}'
                         for name in self.__dict__)
        return f'Configuration({args})'

    def print(self) -> None:
        for key, value in self.__dict__.items():
            if key == '_mpi_implementation':
                key = 'mpi_implementation'
                value = self.mpi_implementation
            elif key == 'nodes':
                print('nodes')
                for name, dct in value:
                    print(f'  {name:10}{dct}')
                continue
            print(f'{key:18} {value}')

    @property
    def mpi_implementation(self) -> str:
        """Guess MPI implementation: intel or openmpi.

        The intel implementation uses::

            mpiexec --env NAME VAL

        instead of::

            mpiexec -x NAME=VAL

        """
        if self._mpi_implementation is None:
            output = subprocess.check_output([self.mpiexec, '-V']).lower()
            if b'intel' in output:
                self._mpi_implementation = 'intel'
            else:
                self._mpi_implementation = 'openmpi'
        return self._mpi_implementation

    @classmethod
    def read(self, start: Path = None) -> 'Configuration':
        """Find nearest .myqueue/config.py and read it."""
        if start is None:
            start = Path.cwd()
        home = find_home_folder(start)
        config_file = home / '.myqueue' / 'config.py'
        dct: Dict[str, Dict[str, Any]] = {}
        exec(compile(config_file.read_text(), str(config_file), 'exec'), dct)
        cfg = dct['config']
        if 'scheduler' not in cfg:
            raise ValueError(
                'Please specify type of scheduler in your '
                f'{home}/.myqueue/config.py '
                "file (must be 'slurm', 'lfs', 'pbs' or 'test').  See "
                'https://myqueue.rtfd.io/en/latest/configuration.html')

        if 'mpi' in cfg:
            warnings.warn(
                'The "mpi" keyword has been deprecated. '
                'Please remove it or rename to "mpi_implementation"')
            cfg['mpi_implementation'] = cfg.pop('mpi')

        config = Configuration(**cfg, home=home)
        return config


def find_home_folder(start: Path) -> Path:
    """Find closest .myqueue/ folder."""
    f = start
    while True:
        dir = f / '.myqueue'
        if dir.is_dir():
            return f.absolute().resolve()
        newf = f.parent
        if newf == f:
            break
        f = newf
    raise ValueError('Could not find .myqueue/ folder!')


def guess_scheduler() -> str:
    """Try different scheduler commands to guess the correct scheduler."""
    import subprocess
    scheduler_commands = {'sbatch': 'slurm',
                          'bsub': 'lsf',
                          'qsub': 'pbs'}
    commands = []
    for command in scheduler_commands:
        if subprocess.run(['which', command],
                          stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL).returncode == 0:
            commands.append(command)
    if commands:
        if len(commands) > 1:
            raise ValueError('Please specify a scheduler: ' +
                             ', '.join(scheduler_commands[cmd]
                                       for cmd in commands))
        scheduler = scheduler_commands[commands[0]]
    else:
        scheduler = 'local'
    return scheduler


def guess_configuration(scheduler_name: str = '',
                        queue_name: str = '',
                        in_place: bool = False) -> None:
    """Simple auto-config tool.

    Creates a config.py file.
    """
    from .scheduler import get_scheduler
    from .utils import str2number, mqhome

    folder = mqhome() / '.myqueue'
    if not folder.is_dir():
        folder.mkdir()

    name = scheduler_name or guess_scheduler()
    scheduler = get_scheduler(Configuration(scheduler=name))
    nodelist, extra_args = scheduler.get_config(queue_name)
    nodelist.sort(key=lambda ncm: (-ncm[1], str2number(ncm[2])))
    nodelist2: List[Tuple[str, int, str]] = []
    done: Set[int] = set()
    for name, cores, memory in nodelist:
        if cores not in done:
            nodelist2.insert(len(done), (name, cores, memory))
            done.add(cores)
        else:
            nodelist2.append((name, cores, memory))

    cfg: Dict[str, Any] = {'scheduler': scheduler.name}

    if nodelist2:
        cfg['nodes'] = [(name, {'cores': cores, 'memory': memory})
                        for name, cores, memory in nodelist2]
    if extra_args:
        cfg['extra_args'] = extra_args

    text = f'config = {cfg!r}\n'
    text = text.replace('= {', '= {\n    ')
    text = text.replace(", 'nodes'", ",\n    'nodes'")
    text = text.replace(", 'extra_args'", ",\n    'extra_args'")
    text = text.replace('(', '\n        (')
    text = '# generated with mq config\n' + text

    if in_place:
        cfgfile = folder / 'config.py'
        if cfgfile.is_file():
            cfgfile.rename(cfgfile.with_name('config.py.old'))
        cfgfile.write_text(text)
    else:
        print(text)
